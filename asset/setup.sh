#!/bin/sh

# Install library
yum clean all
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
yum -y install deltarpm
yum -y groupinstall base "Development and Creative Workstation" --setopt=group_package_types=mandatory,default,optional

# Deploy Eclipse
tar xvzf /asset/eclipse-inst-linux64.tar.gz -C /opt/
ln -s /opt/eclipse-installer /usr/bin

wget -q -O /tmp/pleiades.zip http://ftp.jaist.ac.jp/pub/mergedoc/pleiades/build/stable/pleiades.zip
unzip /tmp/pleiades.zip -d /tmp/pleiades

# Install JDK
yum -y localinstall /asset/jdk-8u101-linux-x64.rpm